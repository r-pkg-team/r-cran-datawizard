Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: datawizard
Upstream-Contact: Indrajeet Patil <patilindrajeet.science@gmail.com>
Source: https://cran.r-project.org/package=datawizard
Files-Excluded: */inst/doc/*.html

Files: *
Copyright: 2019-2021 Dominique Makowski (<https://orcid.org/0000-0001-5375-9967>,
   @Dom_Makowski),
 Daniel Lüdecke (<https://orcid.org/0000-0002-8895-3206>,
   @strengejacke),
 Indrajeet Patil (<https://orcid.org/0000-0003-1995-6531>,
   @patilindrajeets),
 Mattan S. Ben-Shachar,
 Brenton M. Wiernik (<https://orcid.org/0000-0001-9560-6336>,
   @bmwiernik)
License: MIT
 Copyright (c) 2023, datawizard authors
 .
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: debian/*
Copyright: 2021 Andreas Tille <tille@debian.org>
License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 3 at /usr/share/common-licenses/GPL-3.
